''' Template for building up your atoms object from mol2 files '''

import sys
sys.path.append('..')
from autolj import AutoLJ
from mol2 import read_mol2

# Define total system based on 2 mol2 files 
qm = read_mol2('geom.mol2')
qmidx = range(len(qm))
mm = read_mol2('geom.mol2')  # obviously this should be another mol2
mmidx = range(len(qm), len(qm) + len(mm))

atoms = qm + mm 

# init object, give it mol2, this example its the QM subsystem
alj_qm = AutoLJ('geom.mol2', param_file='../parameters/riccardi.prm')
alj_mm = AutoLJ('geom.mol2', param_file='../parameters/riccardi.prm')


# Get QM/MM eps and sigma arrays
eps_qm, sig_qm = alj_qm.get_lj(atoms[qmidx])
eps_mm, sig_mm = alj_mm.get_lj(atoms[mmidx])

print(eps_qm)
print(sig_qm)


