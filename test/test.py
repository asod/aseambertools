import sys
sys.path.append('..')
from autolj import AutoLJ
from ase.io import read

# init object, give it mol2, this example its the QM subsystem
alj = AutoLJ('geom.mol2', param_file='../parameters/riccardi.prm')

# read in the whole system
atoms = read('corresponding.xyz')
qmidx = range(9)

# Get QM/MM eps and sigma arrays
eps_qm, sig_qm = alj.get_lj(atoms[qmidx])

print(eps_qm)
print(sig_qm)


## HOW TO SKIP THE AUTOTYPING: 
# import same things

# init object, this time switching off autotyping, and manually specifying
# ac name (which, in this case, is the same name)
alj = AutoLJ('geom.mol2', param_file='../parameters/riccardi.prm', do_typing=False,
             ac_name='geom.ac')

# the rest is the same
atoms = read('corresponding.xyz')
qmidx = range(9)

eps_qm, sig_qm = alj.get_lj(atoms[qmidx])

print(eps_qm)
print(sig_qm)
