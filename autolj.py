import numpy as np
import os
import subprocess as sp
import sys

class AutoLJ:
    ''' Automatic assignment of QM/MM LJ Parameters based on 
        GAFF-style atomtypes in mol2 file. 
        
        Uses the free AmberTools package for atom typing. 
        conda install ambertools=19 -c ambermd

        NB1: atomtypes is NOT perfect, if it can't type an atom
             it assigns it the 'dummy' label (du)
             You can have it skip the auto-typing by do_typing = False

        NB2: You have to MANUALLY CHECK that the bonding in your 
             mol2 MAKES SENSE!

        molfile: str
            path to .mol2 file

        param_file: str
            path to the LJ-parameter file (in ASE units!)

        do_typing: Logical 
            True:  Uses the AmberTools atomtypes script to assign labels 
                   from mol2
            False: Work directly from .ac file with the same name as .mol2

        careful: Logical
            How much to break everything if parameters might be misassigned
        
        ac_name: str
            The name of the ac file (if not autotyping)
    '''

    def __init__(self, molfile, param_file='parameters/riccardi.prm',
                 do_typing=True, careful=True, ac_name=None):
        self.do_typing = do_typing
        if 'AMBERHOME' in os.environ:
            atomtype = os.environ['AMBERHOME'] + os.sep + 'bin' + os.sep + 'atomtype'
        elif os.system('which atomtype') == 0:  # executable already in path
            atomtype = 'atomtype'
        else:  # AmberTools not installed, assuming the .ac file is acquired elsewhere
            atomtype = None
            self.do_typing = False
            if careful:
                print('AmberTools not found - skipping conversion, assuming ac file exists')

        self.atomtype = atomtype
        self.molfile = molfile
        self.param_file = param_file
        self.careful = careful
        self.ac_name = ac_name


    def type_atoms(self):
        ac_name = self.ac_name
        if not ac_name:
            ac_name = self.molfile.replace('mol2', 'ac')
        cmd = [self.atomtype, '-i',self.molfile,
                              '-o', ac_name,
                              '-f','mol2','-p','gaff','-a','1']

        p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        out, err = p.communicate()
        if len(out) > 0:
            print('ATOMTYPES FAILED:')
            for o in out.decode('UTF-8').split('\n'):
                print(o)
            for e in err.decode('UTF-8').split('\n'):
                print(o)

    def parse_ac(self):
        ac_name = self.ac_name
        if not ac_name:
            ac_name = self.molfile.replace('mol2', 'ac')
        with open(ac_name) as f:
            lines = f.readlines()

        atms = []
        for line in lines:
            if line.startswith('ATOM'):
                atms.append(line.split())
       
        # lets hope the .ac format doesn't change
        self.ac_pos = np.array(atms)[:, 5:8].astype(float)
        self.atypes = np.array(atms)[:, 9]
        atypes = self.atypes.tolist()

        self.atypes = np.array(atypes)

        if self.careful:
            assert (not 'du' in self.atypes), 'DUMMY ATOMS IN AC FILE, GO FIX THEM!'
        if 'du' in self.atypes:
            print('WARNING: DUMMY ATOMS IN AC FILE')


    def read_params(self):
        with open(self.param_file, 'r') as f:
            lines = f.readlines()
        d = {}
        for line in lines:
            if len(line.strip()) == 0:
                continue
            if not line.startswith('#'):
                key = line.split()[0]
                eps = float(line.split()[1])
                sig = float(line.split()[2])
                d[key] = (eps, sig)

        self.params = d

    def get_lj(self, atoms):  # atoms used for check

        if self.do_typing:
            self.type_atoms()

        self.parse_ac()

        self.read_params()

        assert len(atoms.positions) == len(self.ac_pos),\
                'atoms and mol2 have different lengths!' 

        pos_changes = atoms.get_positions() - self.ac_pos
        if self.careful:
            assert((pos_changes < 1e-2).all()),\
                  'atoms and .mol2 positions NOT THE SAME - NO LJs ASSIGNED'
        else:
            if (pos_changes < 1e-2).all():
                print('WARNING: atoms and .mol2 positions NOT EQUAL')

        eps = np.zeros(len(atoms))
        sig = np.zeros(len(atoms))
        
        assert(len(self.atypes) == len(atoms)),\
                'NUMBER OF ATOMS NOT EQUAL TO NUMBER OF TYPED ATOMS' 

        for i, atype in enumerate(self.atypes):
            try:
                eps[i], sig[i] = self.params[atype]
            except:
                print('NO PARAMETER FOR ' + atype)
                eps[i] = 0 
                sig[i] = 0
                if self.careful:
                    sys.exit()

        return eps, sig


