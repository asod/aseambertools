**autolj.py**
-------------
Uses the free AmberTools package for atom typing.   
Conda users can do: conda install ambertools=19 -c ambermd

**NB1:** atomtypes is NOT perfect, if it can't type an atom it assigns it the 'dummy' label (du)

**NB2:** You have to MANUALLY CHECK that the bonding in your .mol2 makes sense! Especially if the connectivity in the file is created by 'automatic guesswork' like with openbabel


See ./test/test.py for a usage example. 



WIP: User-friendly way of re-assigning dummy-labeled atoms. 

**mol2.py**
-------------
Simple .mol2-to-ASE atoms object. see ./test/test2.py