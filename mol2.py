import numpy as np
from ase import Atoms

def read_mol2(fname):
    with open(fname, 'r') as f:
        lines = f.readlines()

    elements = []
    positions = []

    for l, line in enumerate(lines):
        if '@<TRIPOS>ATOM' in line:
            ct = 1 
            while not '@' in lines[l + ct]:
                tl = lines[l + ct]
                
                el = tl.split()[1]
                x = float(tl.split()[2])
                y = float(tl.split()[3])
                z = float(tl.split()[4])

                elements.append(el)
                positions.append([x, y ,z])
                ct += 1

    pos = np.array(positions)
    atoms = Atoms(symbols=elements, positions=pos)

    return atoms



